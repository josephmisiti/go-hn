A quick program, I hacked together to browse [HackerNews](http://news.ycombinator.com) using the command line.

### Installation
You need to have [Phantomjs](http://phantomjs.org/) installed and in PATH.

In order to build, you need to have [Go](http://golang.org) installed.

Use the `go` tool to fetch the source and build the executable
`go get gitlab.com/shank/go-hn`

You should have the binary in `$GOPATH/bin`. In case you have `$GOPATH/bin` in your PATH, you can run the program directly from the command line using
`go-hn --username your_username --password your_password`

### Controls
- right key - go to next page
- left key - go to previous page
- `Ctrl+u` - Upvote
- `enter` - Open the post link in default browser
- `Ctrl+c` - Quit the application

### TODO
This is just a quick hack and is no way polished. I will polish it and add features, as and when I get some spare time.

### Credits & Acknowledgements
Under the hood, this program is using the following awesome tools and utils:
- [gocui](https://github.com/jroimartin/gocui): Minimalist Go package aimed at creating Console User Interfaces
- [agouti](https://github.com/sclevine/agouti): A WebDriver client and acceptance testing library for Go
- [goquery](https://github.com/PuerkitoBio/goquery): A little like that j-thing, only in Go.